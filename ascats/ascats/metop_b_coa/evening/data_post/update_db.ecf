%manual
Description of this task
%end

import os
import sys
import datetime
import logging
import psycopg2

from ecflowrun.context.manager import EcflowContextManager
from ecflowrun.utils import TemporaryDirectory

context = EcflowContextManager(
    ECF_PORT='%ECF_PORT%',
    ECF_NODE='%ECF_NODE%',
    ECF_NAME='%ECF_NAME%',
    ECF_PASS='%ECF_PASS%',
    ECF_TRYNO='%ECF_TRYNO%',
    LOGGER='%ECF_NAME%'
)

db = psycopg2.connect(
    database='%DB_DATABASE%',
    user='%DB_USER%',
    password='%DB_PASS%',
    host='%DB_HOST%'
)

QUERY = """
DELETE FROM %DB_TABLE%
WHERE datetime < '{}';
"""

with context:
    context.log('Updating database: entries older than 72 hours will be deleted.', logging.INFO)
    dt_filter = datetime.datetime.utcnow()
    dt_filter -= datetime.timedelta(hours=72)

    context.log('Entries before {} will be deleted.'.format(dt_filter.strftime('%%Y-%%m-%%d %%H:%%M')), logging.INFO)
    cursor = db.cursor()
    cursor.execute(QUERY.format(dt_filter.strftime('%%Y-%%m-%%d %%H:%%M')))
    db.commit()

    context.log('Finished updating database entries.', logging.INFO)
    cursor.close()
    db.close()
