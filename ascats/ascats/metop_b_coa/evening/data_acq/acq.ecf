%manual
Description of this task
%end

import os
import sys
import datetime
import logging
import ftplib

from ecflowrun.context.manager import EcflowContextManager
from ecflowrun.utils import TemporaryDirectory

context = EcflowContextManager(
    ECF_PORT='%ECF_PORT%',
    ECF_NODE='%ECF_NODE%',
    ECF_NAME='%ECF_NAME%',
    ECF_PASS='%ECF_PASS%',
    ECF_TRYNO='%ECF_TRYNO%',
    LOGGER='%ECF_NAME%'
)


ARCHIVE = {
    'host': '%MAPS_HOST%',
    'user': '%MAPS_USER%',
    'pass': '%MAPS_PASS%',
    'dir': '%MAPS_DIR%'
}


with context:
    ftp = ftplib.FTP(ARCHIVE['host'], ARCHIVE['user'], ARCHIVE['pass'])
    ftp.cwd(ARCHIVE['dir'])

    today = datetime.datetime.utcnow()
    search_string = today.strftime('%PRODUCT%-%%Y-%%m-%%dT*.geojson')
    context.log(search_string, logging.INFO)

    results = ftp.nlst(search_string)

    if len(results) < 1:
        context.log('No data for product %PRODUCT% has been found', logging.INFO)
        context.event('data_not_found')
    else:
        context.event('data_found')
        with TemporaryDirectory(preserve=True, prefix=os.path.basename('%PRODUCT%')) as tmpd:
            for result in results:
                context.log('Fetching file {}'.format(result), logging.INFO)
                with open(os.path.join(tmpd.path, result), 'wb') as localfile:
                    ftp.retrbinary('RETR {}'.format(result), localfile.write, 1024)
        context.event('acq_finished')

    context.label('last_run', datetime.datetime.utcnow())
    context.log('Finishing...', logging.INFO)
