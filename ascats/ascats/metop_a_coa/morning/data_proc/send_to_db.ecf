%manual
Description of this task
%end

import os
import sys
import datetime
import logging
import ftplib
import psycopg2
import json
from shapely import geometry

from ecflowrun.context.manager import EcflowContextManager
from ecflowrun.utils import TemporaryDirectory

context = EcflowContextManager(
    ECF_PORT='%ECF_PORT%',
    ECF_NODE='%ECF_NODE%',
    ECF_NAME='%ECF_NAME%',
    ECF_PASS='%ECF_PASS%',
    ECF_TRYNO='%ECF_TRYNO%',
    LOGGER='%ECF_NAME%'
)

db = psycopg2.connect(
    database='%DB_DATABASE%',
    user='%DB_USER%',
    password='%DB_PASS%',
    host='%DB_HOST%'
)

QUERY = """
INSERT INTO %DB_TABLE%
VALUES (DEFAULT, '{timestamp}', {wind_s}, {wind_dir}, '{datetime_real}', ST_GeometryFromText('{point}', 4326));
"""


with context:
    context.log('Establishing database connection...', logging.INFO)
    cursor = db.cursor()
    context.log('Database connection established...', logging.INFO)

    with TemporaryDirectory(preserve=True, prefix=os.path.basename('%PRODUCT%')) as tmpd:
        files_list = os.listdir(tmpd.path)
        for localfile in files_list:
            context.log('Processing file {}'.format(localfile), logging.INFO)
            with open(os.path.join(tmpd.path, localfile), 'r') as fp:
                data = json.load(fp)

            timestamp = datetime.datetime.strptime(os.path.basename(localfile)[-23:], '%%Y-%%m-%%dT%%H%%M.geojson')
            for feature in data['features']:
                insert = {
                    'point': geometry.Point(*feature['geometry']['coordinates']),
                    'wind_s': feature['properties']['wind_s'],
                    'wind_dir': feature['properties']['wind_dir'],
                    'timestamp': timestamp.strftime('%%Y-%%m-%%dT%%H:%%M'),
                    'datetime_real': '{}:{}'.format(str(int(feature['properties']['hour'])), str(int(feature['properties']['min'])))
                }
                try:
                    cursor.execute(QUERY.format(**insert))
                except psycopg2.IntegrityError as e:
                    db.rollback()
                else:
                    db.commit()

        context.log('Processing finished for all files.', logging.INFO)

    context.log('Closing database connection...', logging.INFO)
    cursor.close()
    db.close()
    context.log('Database connection closed. Exiting...', logging.INFO)
