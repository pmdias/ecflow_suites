Installation
============

Installation of the netCDF4 packages

    sudo apt-get install libhdf5-dev libnetcdf-dev python-dev
    pip install netcdf4

Installation of the Scipy package

    sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran
    pip install scipy
